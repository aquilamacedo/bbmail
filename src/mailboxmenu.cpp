// mailboxmenu.cpp for bbmail - an tool for display the mail in X11.
//
//  Copyright (c) 1998-2005 by John Kennis, jkennis@chello.nl
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// (See the included file COPYING / GPL-2.0)
//
//
#include "mailboxmenu.h"

MailboxMenu::MailboxMenu(ToolWindow *toolwindow) :
        Menu(*toolwindow, toolwindow->getCurrentScreen()), bbtool(toolwindow)
{
    unsigned int i;

    hideTitle();

    for (i = 0; i < bbtool->getResource()->report.number_of_spoolfiles; i++) {
        insertItem(bbtool->getResource()->spoolfile[i].name, i);
    }
    id_reconfigure = bbtool->getResource()->report.number_of_spoolfiles;
    id_close = bbtool->getResource()->report.number_of_spoolfiles + 1;
    insertSeparator();
    bt::ustring str = bt::toUnicode("Reconfigure");
    insertItem(str, id_reconfigure);
    str = bt::toUnicode("Close");
    insertItem(str, id_close);
}


MailboxMenu::~MailboxMenu(void)
{
}

void MailboxMenu::refresh()
{
    static int check_id = 0;
    if (bbtool->checkMail()->getNewmail()) {
        setItemChecked(check_id, false);
        check_id = bbtool->checkMail()->getNewMailbox();
        setItemChecked(check_id, true);
    }
}

void MailboxMenu::hideAll(void)
{
    bbtool->envelopeWindow()->release();
    bt::Menu::hideAll();
}

void MailboxMenu::reconfigure()
{
    unsigned int i;
    clear();
    for (i=0; i < bbtool->getResource()->report.number_of_spoolfiles; i++) {
        insertItem(bbtool->getResource()->spoolfile[i].name, i);
    }
    id_reconfigure = bbtool->getResource()->report.number_of_spoolfiles;
    id_close = bbtool->getResource()->report.number_of_spoolfiles + 1;
    insertSeparator();
    bt::ustring str = bt::toUnicode("Reconfigure");
    insertItem(str, id_reconfigure);
    str = bt::toUnicode("Close");
    insertItem(str, id_close);
    bt::Menu::reconfigure();
}

void MailboxMenu::popup(int x, int y, bool withdrawn)
{
    if (!withdrawn) {
/*        bt::Rect constrain(bbtool->getCurrentScreenInfo()->width(), 0, 0 , 0);
        int y_old=y;
        y = y - rect().height() - 1;
        if ((x + rect().width()) > bbtool->getCurrentScreenInfo()->width())
            x = bbtool->getCurrentScreenInfo()->width() - rect().width();
        if (y < 0)
            y = y_old + bbtool->frameWindow()->height();*/
        bt::Menu::popup(x, y);// constrain);
    } else {
/*        bt::Rect constrain(0, bbtool->getCurrentScreenInfo()->height(), 0, 0);
        x = x - rect().width() - 6;
        if (x<0)
            x = x + rect().width() + 12;
        if (y<0)
            y=0;
        if (y + rect().height() > bbtool->getCurrentScreenInfo()->height())
            y = bbtool->getCurrentScreenInfo()->height() - rect().height();*/
        bt::Menu::popup(x, y);//, constrain);
    }
}


void MailboxMenu::UpdateNumbers(int newmail, int totalmail,int id)
{
    char dummy[256];

    sprintf(dummy," (%d/%d)", newmail, totalmail);
    bt::ustring str = bt::toUnicode(dummy);
    changeItem(id, bbtool->getResource()->spoolfile[id].name + str);

    if (newmail > 0)
        setItemChecked(id, true);
    else
        setItemChecked(id, false);
    //reconfigure();
}

void MailboxMenu::itemClicked(unsigned int id, unsigned int button)
{
    hide();
    if (button == 1) {
        if (id < bbtool->getResource()->report.number_of_spoolfiles) {
            if (!bbtool->getResource()->spoolfile[id].runCommand.empty()) {
                system(bbtool->getResource()->spoolfile[id].runCommand.c_str());
            }
            else if (!bbtool->getResource()->report.runCommand.empty()) {
                system(bbtool->getResource()->report.runCommand.c_str());
            }
        } else if (static_cast<int>(id) == id_reconfigure) {
            bbtool->reconfigure();
        } else if (static_cast<int>(id) == id_close) {
            bbtool->shutdown();
        }
        bbtool->envelopeWindow()->envelopePushed(false);
        bbtool->redraw(false);
    }
}

