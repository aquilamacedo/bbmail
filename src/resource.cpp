// resource.cpp for bbmail - an tool to display new mail in X11.
//
//  Copyright (c) 1998-2005 John Kennis, jkennis@chello.nl
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// (See the included file COPYING / GPL-2.0)
//

#include "bbmail.h"
#include "resource.h"
#include "blackboxstyle.h"

Resource::Resource(ToolWindow *toolwindow, const std::string &rc_file):
		BaseResource(*toolwindow, toolwindow->getCurrentScreen(), rc_file), 
    	spoolfile(0),
        bbtool(toolwindow)
{
        report.force_check= false;
		Load();
}

Resource::~Resource()
{
	Clean();
}

void Resource::Clean()
{
	delete [] spoolfile;
}


void Resource::Load(void)
{
	SizeAndPosition();
	Frame();
	Show();
	if (show.label) {
		Label();
	}
	if (show.envelope) {
		Envelope();
	}
	MailCheckSettings();
	MailboxDep();
	loadMenuStyle();
}


void Resource::Frame()
{
	frame.texture = readTexture("bbmail.frame", "Bbmail.Frame", BB_FRAME, BB_C_FRAME,
			            "Raised Gradient Vertical", "slategrey","lightslategrey");

	unsigned int default_bevel = 4;
//	if (bbtool->config().isWithdrawn()) default_bevel = 0;

	frame.bevelWidth = readUInt("bbmail.bevelWidth", "Bbmail.bevelWidth", BB_BEVELWIDTH, 
				    BB_C_BEVELWIDTH, default_bevel);
}

void Resource::SizeAndPosition()
{
	unsigned int w,h;

	if (!(bbtool->config().isWithdrawn())) 
		bbtool->config().setWithdrawn(readBool("bbmail.withdrawn", "Bbmail.Withdrawn", false));

	if (bbtool->config().geometry().empty()) {
		std::string positionstring = readString("bbmail.position","Bbmail.Position", "-0-0");
		position.mask=XParseGeometry(positionstring.c_str(), &position.x, &position.y, &w, &h);
		if (!(position.mask & XValue))
			position.x=0;
		if (!(position.mask & YValue))
			position.y=0;
	}

	bbtool->setHeight(readUInt("bbmail.height", "Bbmail.Height", 0));
	frame.font = readFont("bbmail.heightBy.font", "Bbmail.HeightBy.Font", BB_FONT, 
			      BB_C_FONT);

}



void	Resource::MailCheckSettings()
{
	report.check_delay = readUInt("bbmail.check.delay", "Bbmail.Check.Delay",5);
	report.force_check_delay = readUInt("bbmail.forceCheck.onDelay", "Bbmail.forceCheck.onDelay", 60);
	bbtool->setRaised(readBool( "bbmail.raised", "Bbmail.Raised", true));
	report.auto_raise = readBool("bbmail.autoRaise", "Bbmail.AutoRaise", true);
}


void Resource::Label(void)
{
	/* text-label resources */
	label.transparent = readBool("bbmail.label.transparent", "Bbmail.label.Transparent", false);
	
	label.texture = readTexture("bbmail.label", "Bbmail.Label",BB_LABEL,BB_C_LABEL,
				    "Sunken Gradient Vertical",
				    "slategrey","darkslategrey");

	if (show.newmail_counter) {
	  	label.newmail_textColor = readColor("bbmail.newmail.counter.textColor",
			  		  	    "Bbmail.Newmail.Counter.TextColor",
						    BB_LABEL_TEXTCOLOR, BB_C_LABEL_TEXTCOLOR,
						    "LightGrey");

		label.newmail_boring_textColor = readColor("bbmail.newmail.counter.boring.textColor",
					                   "Bbmail.Newmail.Counter.Boring.TextColor",
						           getColorName(label.newmail_textColor));
	}

	if (show.totalmail_counter) {
		label.totalmail_textColor = readColor("bbmail.totalmail.counter.textColor",
						      "Bbmail.TotalMail.Counter.TextColor",
						       BB_LABEL_TEXTCOLOR, BB_C_LABEL_TEXTCOLOR,
						       "LightGrey");

		label.totalmail_boring_textColor = readColor("bbmail.totalmail.counter.boring.textColor",
			 				     "Bbmail.Totalmail.Counter.Boring.TextColor",
								getColorName(label.totalmail_textColor));
	}

	if (show.newmail_counter & show.totalmail_counter) {
		label.seperator_textColor = readColor("bbmail.seperator.counter.textColor",
						      "Bbmail.Seperator.Counter.TextColor",
						       BB_LABEL_TEXTCOLOR, BB_C_LABEL_TEXTCOLOR,
						       "LightGrey");

		label.seperator_boring_textColor = readColor("bbmail.seperator.counter.boring.textColor",
							     "Bbmail.Seperator.Counter.Boring.TextColor",
							getColorName(label.seperator_textColor));
	}
	
	label.font = readFont("bbmail.label.font", "Bbmail.Label.Font", BB_FONT, BB_C_FONT);
}


void Resource::Show()
{
	report.number_of_digits = readUInt("bbmail.numberOf.digits", "Bbmail.numberOf.digits", 2);
	if (report.number_of_digits==0)
		report.number_of_digits=1;
	
	/* what to show.resources */
	show.newmail_counter = readBool("bbmail.show.newmail.counter", "Bbmail.Display.Newmail.Counter", true);
	show.totalmail_counter = readBool("bbmail.show.totalmail.counter", "Bbmail.Display.Totalmail.Counter", true); 

	if (!( (show.totalmail_counter) | (show.newmail_counter))) show.label=False;
	else show.label=True;

	show.envelope = readBool("bbmail.show.envelope", "Bbmail.Display.Envelope", true);

	show.onlyAtNewMail = readBool("bbmail.show.onlyAtnewmail", "Bbmail.Display.OnlyAtNewmail", false);

	report.runCommand = readString("bbmail.pressed.runCommand","Bbmail.Pressed.RunCommand","");
}


void Resource::Envelope()
{
	if (!(show.onlyAtNewMail)) {

		envelope.texture = readTexture("bbmail.envelope","Bbmail.Envelope",
                                               BB_ENVELOPE, BB_C_ENVELOPE,
					       "Raised Gradient Vertical", "slategrey","lightslategrey");
		
		envelope.texture_pressed = readTexture("bbmail.envelope.pressed",
						       "Bbmail.Envelope.Pressed",
                                                       BB_ENVELOPE_PRESSED, BB_C_ENVELOPE_PRESSED,
                                                       "Raised Gradient Vertical", "slategrey","lightslategrey");

		envelope.textColor = readColor("bbmail.envelope.textColor", "Bbmail.Envelope.TextColor",
						BB_ENVELOPE_TEXTCOLOR, BB_C_ENVELOPE_TEXTCOLOR, "black");

		envelope.error_textColor = readColor("bbmail.envelope.error.textColor",
						     "Bbmail.Envelope.error.TextColor",
						     "red2");


		envelope.bevelWidth = readUInt("bbmail.envelope.bevelWidth",
					      "Bbmail.Envelope.BevelWidth", 2);
		envelope.bevelHeight = envelope.bevelWidth;
//		if (bbtool->config().isWithdrawn()) envelope.bevelHeight = 0;
	}
}


void Resource::MailboxDep(void)
{
	unsigned int i;
	char strname[48];
	char strclass[48];
	bool default_defined=False;
	int start_mailbox=1;

	valid_mailbox = true;
	
	if (!bbtool->config().spoolDir().empty()) {
		report.number_of_spoolfiles = 1;
		delete [] spoolfile;
		spoolfile = new SpoolList[report.number_of_spoolfiles];
		default_defined = true;
	} else {
	  	report.number_of_spoolfiles = readUInt("bbmail.numberOf.mailboxes",
						       "bbmail.NumberOf.Mailboxes", 0);
		if (report.number_of_spoolfiles > 100)
			report.number_of_spoolfiles = 100;
		
		if (bbtool->config().mailboxId()) {
			if (bbtool->config().mailboxId() > report.number_of_spoolfiles) {
				fprintf(stderr,"mailbox %i does not exist\n", bbtool->config().mailboxId());
			}
			start_mailbox = bbtool->config().mailboxId();
			report.number_of_spoolfiles = 1;
		}

		if (report.number_of_spoolfiles!=0) {
			delete [] spoolfile;
			spoolfile = new SpoolList[report.number_of_spoolfiles];
		} else {
			if (!getenv("MAIL")) {
				fprintf(stderr,
					"Error: No spool filename specified.\nMake sure the enviroment variable MAIL is set\n");
				valid_mailbox = false;
				return;
			}
			report.number_of_spoolfiles = 1;
			delete [] spoolfile;
			spoolfile = new SpoolList[report.number_of_spoolfiles];
			default_defined = true;
		}
	}
	std::string type;
	/* read spool file names */
	for (i = 0; i < report.number_of_spoolfiles; i++) {
		/* check MTA type */
	  	const std::string rname = "bbmail.mailbox." + bt::itostring(i+start_mailbox) + ".";
		const std::string rclass = "Bbmail.Mailbox." + bt::itostring(i+start_mailbox) + ".";

		if (default_defined) {
			if (!bbtool->config().spoolDir().empty()) {
				spoolfile[0].filename = bbtool->config().spoolDir();
			} else {
			  	std::string filename = std::string(getenv("MAIL"));
				spoolfile[0].filename = filename;
			}
		} else {
			spoolfile[i].filename = readString(rname + "filename", rclass + "Filename", "");
		}

		type = readString( rname + ".type",rclass + ".Type","mbox");
		if (!strcasecmp(type.c_str(),"maildir")) {
			spoolfile[i].type = SpoolList::maildir;
			spoolfile[i].new_maildir = spoolfile[i].filename + "/new";
			spoolfile[i].cur_maildir = spoolfile[i].filename + "/cur";
		} else if (!strcasecmp(type.c_str(), "mbox")) {
			spoolfile[i].type = SpoolList::mbox;
		} else if (!strcasecmp(type.c_str(), "other")) {
			spoolfile[i].type = SpoolList::other;
		} else {
			spoolfile[i].type = SpoolList::mbox;
		}
		
		spoolfile[i].name = bt::toUnicode(readString(rname + "name", rclass + "name", "mailbox" + bt::itostring(i+start_mailbox)));

		/* new mail */
		if (show.envelope) {
			spoolfile[i].newmail_texture = readTexture(rname + ".newmail", rclass + ".NewMail",
								   BB_NEWMAIL,BB_C_NEWMAIL,
								   "Raised Gradient Diagonal Bevel1",
								   "rgb:c/9/6","rgb:8/6/4");

			spoolfile[i].newmail_pressed_texture = readTexture(rname + "newmail.pressed", 
									   rclass + "Newmail.Pressed",
									   BB_NEWMAIL_PRESSED, BB_C_NEWMAIL_PRESSED,
									   "Sunken Gradient Diagonal Bevel1",
						            		   "rgb:c/9/6","rgb:8/6/4");

			spoolfile[i].newmail_textColor = readColor(rname + "newmail.textColor",rclass + "Newmail.TextColor",
								   BB_ENVELOPE_TEXTCOLOR, BB_C_ENVELOPE_TEXTCOLOR, "black");

			
			spoolfile[i].newmail_error_textColor = readColor(rname + "newmail.error.textColor",
									 rclass + "Newmail.Error.TextColor",
									 "black");

			spoolfile[i].runCommand = readString(rname + "newmail.pressed.runCommand",
						       rclass + "Newmail.Pressed.RunCommand", "");
		} /* end envelope resources */



		spoolfile[i].statusflag = readString(rname + "newmail.statusFlag",rclass + "Newmail.StatusFlag","Status: R");

		if (!default_defined) {
			spoolfile[i].proc = readString(rname + "proc" , rclass + "proc", ""); 
			if (spoolfile[i].filename.empty() && !spoolfile[i].proc.empty()) {
				report.force_check=True;
				spoolfile[i].force_check=True;
			}

			spoolfile[i].reversedProc = readString(rname + "reversedProc", rclass + "ReversedProc", "");
			if (spoolfile[i].filename.empty() && !spoolfile[i].reversedProc.empty()) {
				report.force_check=True;
				spoolfile[i].force_check=True;
			}
		}
		spoolfile[i].newmailProc = readString(rname + "newmail.runCommand", rclass + "Newmail.RunCommand", "");

		spoolfile[i].bell = readBool(rname + "newmail.bell", rclass + "Newmail.Bell", true);

		sprintf(strname,"bbmail.mailbox.%d.mode",i+start_mailbox);
		sprintf(strclass,"Bbmail.Mailbox.%d.Mode",i+start_mailbox);
		int mode = readUInt(rname + "mode", rclass + "Mode", 1);
		switch (mode) {
			case 2:
				spoolfile[i].first_is_new = True;
				spoolfile[i].second_is_total = False;
				spoolfile[i].second_ignore = False;
			break;
			case 3:
				spoolfile[i].first_is_new = False;
				spoolfile[i].second_is_total = True;
				spoolfile[i].second_ignore = False;
			break;
			case 4:
				spoolfile[i].first_is_new = True;
				spoolfile[i].second_is_total = True;
				spoolfile[i].second_ignore = True;
			break;
			default:
				spoolfile[i].first_is_new = True;
				spoolfile[i].second_is_total = True;
				spoolfile[i].second_ignore = False;
			break;
		}
	} /* for */
}
