//  blackboxstyle.hh for bbmail - an tool to display new mail in X11.
//
//  Copyright (c) 1998-2003 John Kennis, jkennis@chello.nl
//
//  this program is free software; you can redistribute it and/or modify
//  it under the terms of the gnu general public license as published by
//  the free software foundation; either version 2 of the license, or
//  (at your option) any later version.
//
//  this program is distributed in the hope that it will be useful,
//  but without any warranty; without even the implied warranty of
//  merchantability or fitness for a particular purpose.  see the
//  gnu general public license for more details.
//
//  you should have received a copy of the gnu general public license
//  along with this program; if not, write to the free software
//  foundation, inc., 675 mass ave, cambridge, ma 02139, usa.
//
// (see the included file copying / gpl-2.0)
//


#ifndef __BLACKBOXSTYLE_HH
#define __BLACKBOXSTYLE_HH

/* mailtool.frame */
#define BB_FRAME "toolbar"
#define BB_C_FRAME "Toolbar"
/* mailtool.label */
#define BB_LABEL "toolbar.label"
#define BB_C_LABEL "Toolbar.Label"
/* mailtool.*.counter.textColor */
#define BB_LABEL_TEXTCOLOR "toolbar.label.textColor"
#define BB_C_LABEL_TEXTCOLOR "Toolbar.Label.TextColor"
/* mailtool.bevelWidth */
#define BB_BEVELWIDTH "bevelWidth"
#define BB_C_BEVELWIDTH "BevelWidth"
/* mailtool.*.font */
#define BB_FONT "toolbar.font"
#define BB_C_FONT "Toolbar.Font"
/* bbmail.menu.frame */
#define BB_MENU "menu.frame"
/* bbmail.menu.highlight.color */
#define BB_MENU_HIGHLIGHT_COLOR "menu.frame.highlightColor"
/* bbmail.menu.textColor */
#define BB_MENU_TEXTCOLOR "menu.frame.textColor"
/* bbmail.menu.highlight.textColor */
#define BB_MENU_HITEXTCOLOR "menu.active.textColor"
#define BB_C_MENU_HITEXTCOLOR "Menu.Active.TextColor"
/* bbmail.menu.justify */
#define BB_MENU_JUSTIFY "menu.alignment"
#define BB_C_MENU_JUSTIFY "Menu.Alignment"
/* bbmail.menu.font */
#define BB_MENU_FONT "menu.frame.font"
#define BB_MENU_C_FONT "Menu.Frame.Font"

/* bbmail.mailbox.*.newmail */
#define BB_NEWMAIL "toolbar.button"
#define BB_C_NEWMAIL "Toolbar.Button"
/* bbmail.mailbox.*.newmail.pressed */
#define BB_NEWMAIL_PRESSED "toolbar.button.pressed"
#define BB_C_NEWMAIL_PRESSED "Toolbar.Button.Pressed"
/* bbmail.envelope */
#define BB_ENVELOPE "toolbar.button"
#define BB_C_ENVELOPE "Toolbar.Button"
/* bbmail.envelope.pressed */
#define BB_ENVELOPE_PRESSED "toolbar.button.pressed"
#define BB_C_ENVELOPE_PRESSED "Toolbar.Button.Pressed"
/* bbmail.mailbox.*.*.textColor */
#define BB_ENVELOPE_TEXTCOLOR "toolbar.button.foregroundColor"
#define BB_C_ENVELOPE_TEXTCOLOR "Toolbar.Button.ForegroundColor"
/* bbmail.menu.bulletStyle */
#define BB_MENU_BULLETSTYLE "menu.bullet"
#define BB_C_MENU_BULLETSTYLE "Menu.Bullet"
/* bbmail.menu.bulletPosition */
#define BB_MENU_BULLETPOSITION "menu.bullet.position"
#define BB_C_MENU_BULLETPOSITION "Menu.Bullet.Position"
#endif /* __BLACKBOXSTYLE_HH */
