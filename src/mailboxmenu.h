// mailboxmenu.h for bbmail - an tool for display the mail in X11.
//
//  Copyright (c) 1998-2005 by John Kennis, jkennis@chello.nl
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// (See the included file COPYING / GPL-2.0)
//
#ifndef __MAILBOXMENU_H
#define __MAILBOXMENU_H

#include "bbmail.h"
#include "Menu.hh"

class ToolWindow;

class MailboxMenu : public bt::Menu
{
public:
	MailboxMenu(ToolWindow *);
	virtual ~MailboxMenu(void);
	void UpdateNumbers(int, int, int);
	void popup(int ,int, bool);
	void reconfigure(void);
	virtual void refresh(void);
	virtual void hideAll(void);

protected:
	virtual void itemClicked(unsigned int, unsigned int);

private:
	ToolWindow *bbtool;
	int id_reconfigure;
	int id_close;
};

#endif /* __MAILBOXMENU_H */

