#!/usr/bin/perl 
# 
# Quickie script to parse fetchmail -c output
# (IMAP proto) for use with bbmail
# 
# Typical fetchmail output is:
# "X messages (Y seen) for foo-user at bar-host."
# 
# wcb 4/15/2000
# 
$newmail = 0;
$totalmail = 0;
$line = <STDIN>;
if ( $line =~ /^(\d+)\s+message/i )
	{
	$totalmail = $1;
	if ( $line =~ /\s+\((\d+)\s+seen\)/i )
		{
		$newmail = $totalmail - $1;
		}
	else
		{
		$newmail = $totalmail;
		}
	}
elsif ( $line =~ / no mail/i )
	{
	$newmail = 0;
	$totalmail = 0;
	} 
else		# not getting expected fetchmail output,
		# so report error 
	{
	$newmail = "error";
	$totalmail = "error";
	}
print ($newmail, " ", $totalmail, "\n");
