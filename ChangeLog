version-0.9.3
- Fixed maildir directory setting via resource file.

version-0.9.2
- Fixed crash when reconfiguring (happens automatically when withdrawn). 
- Fixed too wide/high display of bbmail in slit.

version-0.9.1
Patches provided by Joey Morris, fixing the following problems:
 -default style
 -use bt::textureResource this fixes rendering problems and borders
 -fix bug in envelope drawing after reconfigure

version-0.9.0
- update for blackbox-0.70.0

version-0.8.2
- fixed a small bug in the menu

version-0.8.1
- Some small changes to make bbmail compile with gcc3 
  (Thanks to Mads Martin Joergensen)

version-0.8.0:
- removed the none-blackbox specific configuration file.
- added support for maildir mailboxes.
- added close option in menu
- added man-page

version-0.7.0:
- added support for maildir mailboxes.
- changed config files, you now have to change either mbox, maildir or other.

version-0.6.11:
- menu is now placed entirly on the desktop.

version-0.6.10:
- updated image and display code (copied them from latest blackbox)

version-0.6.9:
- Now optional charachters can be defined (between '[' and ']') in the  
  bbmail.mailbox.*.newmail.statusFlag resource.
  An example for netscape:
  bbmail.mailbox.*.newmail.statusFlag:  X-Mozilla-Status: [89ABCDEF]

version-6.8
- updated NETHints to BlackboxHints so it's possible again to display bbmail 
  without decorations.

version-6.7
- fixed segfault when envelope not shown
- added -m <num> option in main, now you can select which mailbox
  defined in configuration file to use. This makes it possible to start
  different bbmails for different mailboxes (using the same configuration 
  file
- added bbmailparsefm.pl perl script, which put the fetchmail output
  in a format which bbmail can use. See the bbmail.mailbox.1.proc: entry
  in the resource file (Thanks to Bill Beal for writing it)
- fixed check time resources
- added aditional debug info

version-6.6
- fixed another segfault error
- fixed menu placement

version-6.5
- fixed segfault
- fixed default texture

version-6.4
- updated Image.* and BaseDisplay (now all ne gradients should work)

version-6.3
- color, colorTo and gradient are ungrouped. For instance you can use
  the default blackbox colors but define another gradient in the bbmail.bb
  style file.
- fixed bug, mail count is displayed in menu again.
- added bbmail.mailbox.*.name to bbmnail.bb and bbmail.nobb
- default mailbox message ignored
- changed bevelWidth behaviour for placement in slit:
  default frame.bevelWidth is 0 when placed in the slit
  default envelope.bevelWidth = 0 when placed in the slit
version-6.2
- fixed segfault when using -d options 
- Updated INSTALL

version-0.6.1
- wmclass=bbtools; wmname=bbmail
- setting -geometry works with -d option
- menu is placed on correct position when using -d option
- resizing decorated window disabled

version-0.6.0
- updated image code
- changed configuration style
- added: restore acces time after spoolfile is read. 
  Some mail programs (for instance mutt) use this to 
  detect if new mail has arrived (Thanks to Eric Davis for the patch).
- changed bbmail.mailbox.*.firstInput, bbmail.mailbox.*.secondInput to 
  bbmail.mailbox.*.mode (see bbmail.bb or bbmail.nobb)
- added support for decorated (normal) window

version-0.5.3
- fixed bug for HP-UX

version-0.5.2
- added resource numberOf.digits so you can define the max number displayed.

version-0.5.1
- fixed bug for 8-bit (static) color-mode
- added working resources withdrawn and shape (for placing bbmail in the slit)

version-0.5.0
- rewrite of code

version 0.4.7
- fixed segfault when not showing label
- updated Image.cc (from Blackbox-0.50.4) now 8-bits display should 
  be better supported.
- config files can be defined as command-line options again
- when one mailbox has no more new mail, the next mailbox with 
  new mail is shown.

version 0.4.6
- small bug in positioning the menu when in the slit fixed.

version 0.4.5
- menu now works with the Slit
- pressed button is shown when menu is open
- when withdrawn bbmail always auto-reconfigures

version 0.4.4
- fixed menu so reconfigure works
- default for new-mail envelope now is the same as blackbox toolbar button

version 0.4.3
- added menu support (disabled when in the Slit)
- added bmail.label.transparent option.
- some bug fixes

version 0.4.2
- added support for different external mail-info formats (now fetchmail is supported)
- fixed auto-reconfigure
- fixed force check changed resource to bbmail.forceCheck.onDelay

version 0.4.1
- changed use of external mail-check program so pop3 is supported.
- buf fix in code external mail-check

version 0.4.0
- added supprt for Blackbox Slit:
	-w : withdrawn in Slit (docked)
	-s : don't show frame window.
- changed configuration style (better supporting Blackbox styles)
- removed audio patch and replaced it by the possibility to execute 
  command when new mail has arrived. 
  Can be used to play audio when new mail has arrived.
- Some additional bug-fixes

version 0.3.6 (internal)
- implemented audio patch by Alex Law <alaw@metapath.com>, which
  made it possible to play sound when new mail arrived.

version 0.3.5
- small bugfix, textColors are now correctly copied from bbtools-style file

version 0.3.4
- bug fixed, counting mail through external program now works.
- bug in bbtools.rc fixed, name bbtools.styleFile: changed to
  bbtools.mailtool.styleFile so different style files can be used 
  for different bbtools.

version 0.3.3
- bug fixed, counting mail through external program should now work.

version 0.3.2
- add resources for boring colors (the color when counter is zero) 

version 0.3.1
- fixed error, 'mail counting' using external progra, should now work.
- added error report, a cross is placed on the envelope when a mailbox
  can't be read.
- changed data files so now the resource names are correct.
- changed resource naming sceme of mailtool.mailbox.*  

version 0.3.0
- added support for multiple mailboxes
- added support for reading Blackbox style
- added support to count the mail using external program (eg. flist)
- added automatic update when style-file is updated, you can now
  change the style.
- added support for different 'Status:' reports in spoolfiles.
- Spooldir can now be defined as command-line option
- changed the configure scripts
- added global configuration files

version 0.2.0
- added resources to choose displaying:
	- # of total mails
	- # of new mails
	- envelope
- added resource make it possible to only display when new mail has arrived
- added possibility to start program when envelope is pushed.
- some thing I have forgotten


version 0.1.1 (internal)
- fixed 'segmentation fault' bug when no spool-file was defined.
- added resource to change color of lines in envelope
- fixed (I hope) configure script
- bbmail now installs in usr/local/bin
- changed names of some resources

05-11-1998 first public version of bbmail (version 0.1.0)
